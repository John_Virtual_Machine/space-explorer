﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.UI;

public class SphereController : MonoBehaviour {

	private Transform _trfm;
	private Rigidbody2D _rb2D;
	
	private bool _grabbed;
	
	public int Id;

	public LevelGameController GameController;
	
	public float MaxSpeed = 1f;
	public float YLimit = 3f;
	
	private float _minY;
	private float _maxY;
	private float _direction = 1;
	
	public AudioClip TreasureSoundClip;
	public AudioSource SoundSource;

	public String Ressource;

	private void Awake()
	{
		_trfm = GetComponent<Transform>();
		_rb2D = GetComponent<Rigidbody2D>();
		
		_minY = _trfm.position.y - YLimit;
		_maxY = _trfm.position.y + YLimit;
	}


	private void Start()
	{
		//if (GameController.IsFound(Id))
		//{
		//	GameController.MarkATreasureFound();
		//	Destroy(gameObject);
		//}
	}

	public String GetRessourcePath()
	{
		Debug.Log(Application.streamingAssetsPath + "/" + Ressource);
		return @Application.streamingAssetsPath + "/" + Ressource;
	}
	
	private void FixedUpdate () {
		Move();
	}
	
	private void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("Player") && !_grabbed)
			Found();
        
	}
	
	private void Move()
	{
		if (IsOffLimit())
			_direction = -_direction;

		_rb2D.velocity = new Vector2(0, MaxSpeed * _direction); 
	}

	private void Found()
	{
		_grabbed = true;
		
		SoundSource.PlayOneShot(TreasureSoundClip);
		GetComponent<Renderer>().enabled = false;
		
		GameController.TreasureFound(Id, gameObject);

	}
	
	private bool IsOffLimit()
	{
		return _trfm.position.y < _minY || _trfm.position.y > _maxY;
	}
}
