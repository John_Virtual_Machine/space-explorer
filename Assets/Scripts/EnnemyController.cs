﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyController : MonoBehaviour {

	public float MaxSpeed = 5f;
	public float XLimit = 10f;
	
	private float _minX;
	private float _maxX;
	private float _direction = 1;
	
	private Transform _trfm;
	private Rigidbody2D _rb2D;
	private Animator _animator;

	private bool _dead;
	
	public AudioClip[] HitVoiceClips;
	public AudioSource VoiceSource;

	private void Start ()
	{
		_trfm = GetComponent<Transform>();
		_rb2D = GetComponent<Rigidbody2D>();
		_animator = GetComponent<Animator>();

		_minX = _trfm.position.x - XLimit;
		_maxX = _trfm.position.x + XLimit;

	}
	
	private void OnCollisionEnter2D(Collision2D other) 
	{
		if (IsCollisionPlayerWhileAttacking(other))
			Die();
	}

	private void FixedUpdate () {
		if (!_dead)
			Move();
	}
	
	private void Move()
	{
		if (IsOffLimit())
		{
			FlipFacing();
			_direction = -_direction;
		}
		_rb2D.velocity = new Vector2(MaxSpeed * _direction, _rb2D.velocity.y);
	}
	
	private void Die()
	{
		var voiceNumber = Random.Range(0, 1);
		VoiceSource.PlayOneShot(HitVoiceClips[voiceNumber]);
		
		_animator.SetBool("Dead", true);
		_dead = true;
	}

	public void Destroy()
	{
		Destroy(gameObject);
	}
	
	private static bool IsCollisionPlayerWhileAttacking(Collision2D other)
	{
		return other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<HeroController>().IsAttacking();
	}

	private bool IsOffLimit()
	{
		return _trfm.position.x < _minX || _trfm.position.x > _maxX;
	}

	private void FlipFacing()
	{
		_trfm.localScale = new Vector3 (
			_trfm.localScale.x * -1, 
			_trfm.localScale.y, 
			_trfm.localScale.z
		);
	}
}
