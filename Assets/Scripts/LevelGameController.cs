﻿using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using LitJson;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

public class LevelGameController : MonoBehaviour {
	
	public Text MiddleText;
	public Text LeftBottomText;
	
	public int TreasureNumber = 3;
	private int _treasuresFound = 0;

	public GameObject PauseMenu;
	
	private JsonData _save;
	
	private void Awake() {
		
		LoadGame();
		
		UpdateLeftBottomText();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) 
			PauseMenu.SetActive (!PauseMenu.active);
	}
	public void MenuNavigation()
	{
		Debug.Log("back to menu");
		SceneManager.LoadScene("Main_Title");
	}
	
	private string GetNameProperty(int treasureId)
	{
		return (string) _save["documents"][treasureId - 1]["name"];
	}
	
	public bool IsFound(int treasureId)
	{
		return (bool) _save["documents"][treasureId - 1]["found"];
	}

	public void TreasureFound(int treasureId, GameObject treasureGameObject)
	{
		MiddleText.text = GetNameProperty(treasureId) + " trouvé !";
		MiddleText.gameObject.SetActive(true);

		MarkATreasureFound();
		
		
		Process.Start(treasureGameObject.GetComponent<SphereController>().GetRessourcePath());
		
		StartCoroutine(WaitTreasureDestroy(4f, treasureGameObject));
	}

	public void MarkATreasureFound()
	{
		_treasuresFound++;
		UpdateLeftBottomText();
	}

	private void LoadGame()
	{
		var fileContent =
			"{\"documents\":[{\"id\":1,\"name\":\"CV\",\"found\":true},{\"id\":2,\"name\":\"Mes Attentes\",\"found\":false},{\"id\":3,\"name\":\"Comp\u00E9tences Professionelles\",\"found\":false},{\"id\":4,\"name\":\"Itin\u00E9raire Professionel\",\"found\":false},{\"id\":5,\"name\":\"Formation Compl\u00E9mentaire\",\"found\":false},{\"id\":6,\"name\":\"Traits de Personalit\u00E9\",\"found\":false},{\"id\":7,\"name\":\"Centres d'Int\u00E9rets\",\"found\":false}]}";
		_save = JsonMapper.ToObject(fileContent);
	}

	private void UpdateLeftBottomText()
	{
		LeftBottomText.text = "Documents : " + _treasuresFound + " / " + TreasureNumber;
	}
	
	private IEnumerator WaitTreasureDestroy(float seconds, GameObject treasure){
		yield return new WaitForSeconds(seconds);
		MiddleText.gameObject.SetActive(false);
		Destroy(treasure);
	}
	
}
