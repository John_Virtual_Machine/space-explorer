﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeroController : MonoBehaviour 
{

	public float MaxSpeed = 10f;
	
	private bool _facingRight = true;
	
	private Transform _trfm;
	private Rigidbody2D _rb2D;
	private Animator _animator;

	private bool _grounded;
	public Transform GroundCheck;
	public LayerMask WhatIsGround;
	public float JumpForce;

	private bool _attacking;
	public float AttackForce;

	public AudioClip AttackSoundClip;
	public AudioClip AttackVoiceClip;
	public AudioClip JumpVoiceClip;
	public AudioClip JumpSoundClip;
	public AudioClip DeathSoundClip;
	public AudioClip DeathVoiceClip;
	public AudioClip TreasureVoiceClip;
	public AudioSource SoundSource;
	public AudioSource VoiceSource;

	private void Start ()
	{
		_trfm = GetComponent<Transform>();
		_rb2D = GetComponent<Rigidbody2D>();
		_animator = GetComponent<Animator>();
	}

	private void Update()
	{
		
		if (Input.GetButtonDown("Attack") && !_attacking)
			StartAttack();
		
		if (_grounded && Input.GetButtonDown("Jump") && !_attacking)
			Jump();
	}

	private void FixedUpdate ()
	{
		_grounded = Physics2D.OverlapCircle(GroundCheck.position, 0.2f, WhatIsGround);
		_animator.SetBool("Ground", _grounded);
		
		if (!_attacking)
			Move();
	}
	
	private void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.CompareTag("Spike"))
			Die(other.transform.position);
		
		else if (other.gameObject.CompareTag("Ennemy") && !_attacking)
			Die(other.transform.position);
        
	}

	private void StartAttack()
	{
		var attackForce = _facingRight ? AttackForce : -AttackForce;
		_animator.SetBool("Attack", true);
		_attacking = true;
		
		_rb2D.velocity = Vector2.zero;
		_rb2D.AddForce(new Vector2(attackForce, 0));
		
		
		VoiceSource.PlayOneShot(AttackVoiceClip);
		SoundSource.PlayOneShot(AttackSoundClip);
		StartCoroutine(AttackDuring(0.3f));
	}

	private void StopAttack()
	{
		_animator.SetBool("Attack", false);
		_attacking = false;
		
		_rb2D.velocity = Vector2.zero;
	}

	private void Move()
	{
		var move = Input.GetAxis("Horizontal");
		
		_animator.SetFloat("Speed", Math.Abs(move));
		_rb2D.velocity = new Vector2(move * MaxSpeed, _rb2D.velocity.y);
		
		if (move > 0 && !_facingRight)
			FlipFacing();
		else if (move < 0 && _facingRight)
			FlipFacing();
	}

	private void Jump()
	{
		_animator.SetBool("Ground", false);
		_rb2D.AddForce(new Vector2(0, JumpForce));
		
		SoundSource.PlayOneShot(JumpVoiceClip);
		VoiceSource.PlayOneShot(JumpSoundClip);
	}

	private void Die(Vector2 position)
	{
		_attacking = false;
		_animator.SetBool("Dead", true);
		
		VoiceSource.PlayOneShot(DeathVoiceClip);
		SoundSource.PlayOneShot(DeathSoundClip);

		Debug.Log("Hero is dead.");
		StartCoroutine(ReloadSceneAfter(0.4f));
	}

	private void FlipFacing()
	{
		_trfm.localScale = new Vector3 (
			_trfm.localScale.x * -1, 
			_trfm.localScale.y, 
			_trfm.localScale.z
		);
		_facingRight = !_facingRight;
	}
	
	private IEnumerator AttackDuring(float seconds){
		yield return new WaitForSeconds(seconds);
		StopAttack();
	}

	private IEnumerator ReloadSceneAfter(float seconds){
		yield return new WaitForSeconds(seconds);
		var scene = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(scene, LoadSceneMode.Single);
	}
	
	public bool IsAttacking()
	{
		return _attacking;
	}
}

