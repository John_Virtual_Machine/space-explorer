﻿using System;
using UnityEngine;

public class CameraController : MonoBehaviour
{

	public float XMargin = 1f;
	public float YMargin = 1f;
	public float XSmooth = 8f;
	public float YSmooth = 8f;

	private Transform _player;


	private void Awake()
	{
		_player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	private void FixedUpdate () 
	{
		TrackPlayer();
	}

	private void TrackPlayer()
	{
		var position = transform.position;
		var targetX = position.x;
		var targetY = position.y;

		if (TargetMovedX())
			targetX = Mathf.Lerp(position.x, _player.position.x, XSmooth * Time.deltaTime);
		if (TargetMovedY())
			targetY = Mathf.Lerp(position.y, _player.position.y, YSmooth * Time.deltaTime);
		
		transform.position = new Vector3(targetX, targetY, position.z);
	}
	
	private bool TargetMovedX()
	{
		return Math.Abs(transform.position.x - _player.position.x) > XMargin;
	}
	
	private bool TargetMovedY()
	{
		return Math.Abs(transform.position.y - _player.position.y) > YMargin;
	}
}
