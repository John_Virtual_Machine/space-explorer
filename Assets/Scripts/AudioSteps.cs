﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSteps : MonoBehaviour {
	
	public AudioClip[] StepClips;
	public AudioSource StepSource;


	private void Update()
	{
		if (!StepSource.isPlaying)
		{
			var stepNumber = Random.Range(0, 3);
			StepSource.PlayOneShot(StepClips[stepNumber]);
		}
	}
}
