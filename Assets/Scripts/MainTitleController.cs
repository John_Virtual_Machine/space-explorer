﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainTitleController : MonoBehaviour
{

	public void StartGame()
	{
		Debug.Log("start");
		SceneManager.LoadScene("Game_Level");
	}
	
	public void QuitGame()
	{
		Debug.Log("quit");
		Application.Quit();
	}
}
